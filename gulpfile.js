var gulp = require('gulp'), // Gilp
    watch = require('gulp-watch'), // Watcher
    prefixer = require('gulp-autoprefixer'), // Автопрефиксер
    sass = require('gulp-sass'), // Sass
    sourcemaps = require('gulp-sourcemaps'), // Sourcemap
    browserSync = require("browser-sync"), // reloader browser
    notify = require("gulp-notify"),
    cssmin = require('gulp-minify-css'), // минифиакция css
    reload = browserSync.reload; // для перезагрузки



var path = {    
    public: { //Тут мы укажем куда складывать готовые после сборки файлы
        css: 'css/',
    },
    src: { //Пути откуда брать исходники
        style: 'scss/main.scss',
    },
    watch: { //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
        style: 'scss/**/*.scss',
    },
    clean: './public'
};


var config = { // Сервер для live-reload (Browser-sync)
    server: {
        baseDir: "./"
    },
    //tunnel: true,
    host: 'localhost',
    port: 9000,
    
};


gulp.task('style:build', function () {
    gulp.src(path.src.style) //Выберем наш main.scss
        .pipe(sourcemaps.init()) //То же самое что и с js
        .pipe(sass()) //Скомпилируем
        .on("error", notify.onError(function(error) {
            return "Message to the notifier: " + error.message;
        }))
        .pipe(prefixer(
            {
                browsers: ['last 10 versions'],
                cascade: false
            }
        )) //Добавим вендорные префиксы
        .pipe(cssmin()) //Сожмем
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.public.css)) //И в public
        .pipe(reload({stream: true}));
});


// все таски :build собираем в одинь
gulp.task('build', [
    'style:build',
]);

// подключаем watcher
gulp.task('watch', function(){ 
 
    watch([path.watch.style], function(event, cb) {
        gulp.start('style:build');
    });
    
});

gulp.task('webserver', function () {
    browserSync(config);
});

gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});

gulp.task('default', ['build', 'webserver', 'watch']);