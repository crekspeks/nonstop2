$(function () {

    // $('.header-navigation').addClass('header-navigation_sticky');

    if ($(window).scrollTop() > 160) {
        $('.header-navigation').addClass('header-navigation_sticky');
        $('.slider').addClass('slider-margin');
        $('.menu__pagination').show();
    } else {
        $('.header-navigation').removeClass('header-navigation_sticky');
        $('.slider').removeClass('slider-margin');
        $('.menu__pagination').hide();
    }

    $(window).scroll(function () {

        if ($(this).scrollTop() > 160) {
            $('.header-navigation').addClass('header-navigation_sticky');
            $('.slider').addClass('slider-margin');
            $('.menu__pagination').show();
        } else {
            $('.menu__pagination').hide();
            $('.slider').removeClass('slider-margin');
            $('.header-navigation').removeClass('header-navigation_sticky');
        }

    });

    $('.header-navigation-search__input').focus(function () {
        $('.header-navigation-search').addClass('header-navigation-search_wide');
        $('.header-navigation-menu').addClass('header-navigation-menu_hide');
        $('.header-navigation-search-fields:nth-child(3)').addClass('header-navigation-search-fields_active');
        $('.header-navigation-search').addClass('header-navigation-search_focus');
        $('.overlay').addClass('overlay_active');


        $('.header-navigation-catalog-item__headline').removeClass('header-navigation-catalog-item__headline_active');
        $('.header-navigation-catalog-item__headline').parent().find('div.header-navigation-catalog-item-submenu').removeClass('header-navigation-catalog-item-submenu_active');

        $('.header-navigation-catalog-item__headline').removeClass('header-navigation-catalog__button_focus');
        $('.header-navigation-catalog').removeClass('header-navigation-catalog_active');
    });
    $('.header-navigation-search__input').focusout(function () {
        $('.header-navigation-search').removeClass('header-navigation-search_wide');

        $('.header-navigation-menu').removeClass('header-navigation-menu_hide');
        $('.header-navigation-search-fields').removeClass('header-navigation-search-fields_active');
        $('.header-navigation-search').removeClass('header-navigation-search_focus');
        $('.overlay').removeClass('overlay_active');
    });
    $('.header-navigation__catalog').mouseenter(function () {
        $('.header-navigation-catalog__button').addClass('header-navigation-catalog__button-transform-img');
        $(this).addClass('header-navigation-catalog__button_focus');
        //$('.overlay').addClass('overlay_active');
        $('.header-navigation-catalog').addClass('header-navigation-catalog_active');
        //$(".header-navigation-catalog_active").slideToggle("slow")
    });
    $('.header-navigation-catalog').mouseleave(function () {
        $('.header-navigation-catalog__button').removeClass('header-navigation-catalog__button-transform-img');
        $(this).removeClass('header-navigation-catalog__button_focus');
        $('.overlay').removeClass('overlay_active');
       $('.header-navigation-catalog').removeClass('header-navigation-catalog_active');
    });
    $('.header-navigation__catalog').mouseleave(function () {
        $('.header-navigation-catalog__button').removeClass('header-navigation-catalog__button-transform-img');
        $(this).removeClass('header-navigation-catalog__button_focus');
        $('.overlay').removeClass('overlay_active');
        $('.header-navigation-catalog').removeClass('header-navigation-catalog_active');
    });
    $('.header-navigation-catalog__button').click(function () {
        if ($(this).hasClass('header-navigation-catalog__button_focus')) {
            $('.header-navigation-catalog__button').removeClass('header-navigation-catalog__button-transform-img');
            $(this).removeClass('header-navigation-catalog__button_focus');
            $('.overlay').removeClass('overlay_active');
            $('.header-navigation-catalog').removeClass('header-navigation-catalog_active');
        } else {
            $('.header-navigation-catalog__button').addClass('header-navigation-catalog__button-transform-img');
            $(this).addClass('header-navigation-catalog__button_focus');
            $('.overlay').addClass('overlay_active');
            $('.header-navigation-catalog').addClass('header-navigation-catalog_active');
        }

    });

    $('.header-navigation-catalog-item__headline').on('click', function (event) {
        event.preventDefault();
        $('.header-navigation-catalog-item__headline').removeClass('header-navigation-catalog-item__headline_active');
        if($(this).hasClass('header-navigation-catalog__button-transform-img')){
            $(this).removeClass('header-navigation-catalog__button-transform-img');
            $(this).parent().find('div.header-navigation-catalog-item-submenu').slideUp("normal");
            $(this).parent().find('div.header-navigation-catalog-item-submenu').removeClass('header-navigation-catalog-item-submenu_active');
        }
        else {
            if( $('.header-navigation-catalog-item-submenu').hasClass('header-navigation-catalog-item-submenu_active')){
                $('.header-navigation-catalog-item-submenu_active').slideUp("normal");
                $(this).parent().find('.header-navigation-catalog-item-submenu_active').removeClass('header-navigation-catalog-item-submenu_active');
            }
            $('.header-navigation-catalog-item__headline').removeClass('header-navigation-catalog__button-transform-img');
            $(this).addClass('header-navigation-catalog__button-transform-img');
            $(this).addClass('header-navigation-catalog-item__headline_active');
            $(this).parent().find('div.header-navigation-catalog-item-submenu').slideDown("normal");
             $(this).parent().find('div.header-navigation-catalog-item-submenu').addClass('header-navigation-catalog-item-submenu_active');
        }
    });
    $('.header-navigation-catalog-item-submenu__item').on('click', function (event) {
        $('.header-navigation-catalog-item-submenu__item-submenu').hide();
        $('.header-navigation-catalog-item-submenu__item-submenu').css("width", "0");

        event.preventDefault();
        $(this).parent().find('.header-navigation-catalog-item-submenu__item-submenu').show();
        $(this).parent().find('.header-navigation-catalog-item-submenu__item-submenu').animate({"width": "+=230px"}, "slow") 
    });
    $('.header-navigation-catalog-item-submenu__item-submenu__item').on('click', function (event) {
        $('.header-navigation-catalog-item-submenu__item-submenu__item-submenu').hide();
        $('.header-navigation-catalog-item-submenu__item-submenu__item-submenu').css("width", "0");

        event.preventDefault();
        $(this).parent().find('.header-navigation-catalog-item-submenu__item-submenu__item-submenu').show();
        $(this).parent().find('.header-navigation-catalog-item-submenu__item-submenu__item-submenu').animate({"width": "+=230px"}, "slow") 
    });
    $('.overlay').click(function () {

        if ($('.header-navigation-catalog__button').hasClass('header-navigation-catalog__button_focus')) {
            $(this).removeClass('header-navigation-catalog__button_focus');
            $('.overlay').removeClass('overlay_active');
            $('.header-navigation-catalog').removeClass('header-navigation-catalog_active');
        }

    });
    $('.header-navigation-menu__item_hamburger').mouseenter(function () {
        if(!$(this).find('div').hasClass('hide-hamburger-menu')){
            var div = document.createElement('div');
            $(div).addClass('hide-hamburger-menu');
            $(div).append('<ul style="text-align:right; list-style-type: none;"><li><a href="#" class="header-navigation-menu__link-hide-hamburger-menu">Установка и подключение</a></li><li><a href="#" class="header-navigation-menu__link-hide-hamburger-menu">Как сделать заказ</a></li></ul>');
            $(this).append(div);
        }
    });
    $('.header-navigation-menu__item_hamburger').mouseleave(function () {
        $('.hide-hamburger-menu').remove();
    });
    $('.header-navigation-menu__item_hamburger').click(function () {
        if($(this).find('div').hasClass('hide-hamburger-menu')){
            $('.hide-hamburger-menu').remove();
        }else{
        var div = document.createElement('div');
        $(div).addClass('hide-hamburger-menu');
        $(div).append('<ul style="text-align:right; list-style-type: none;"><li><a href="#" class="header-navigation-menu__link-hide-hamburger-menu">Установка и подключение</a></li><li><a href="#" class="header-navigation-menu__link-hide-hamburger-menu">Как сделать заказ</a></li></ul>');
        $(this).append(div);
        }
        
    });
    $('.header-mobile__button_search').click(function () {
        if ($(this).hasClass('header-mobile__button_search_active')) {
            $(this).removeClass('header-mobile__button_search_active');

            $('.header-navigation-search').removeClass('header-navigation-search_wide');

            $('.header-navigation-menu').removeClass('header-navigation-menu_hide');
            $('.header-navigation-search-fields').removeClass('header-navigation-search-fields_active');
            $('.header-navigation-search').removeClass('header-navigation-search_focus');
            $('.overlay').removeClass('overlay_active');
        } else {
            $(this).addClass('header-mobile__button_search_active');

            $('.header-navigation-search').addClass('header-navigation-search_wide');
            $('.header-navigation-menu').addClass('header-navigation-menu_hide');
            $('.header-navigation-search-fields').addClass('header-navigation-search-fields_active');
            $('.header-navigation-search').addClass('header-navigation-search_focus');
            $('.overlay').addClass('overlay_active');


            $('.header-navigation-catalog-item__headline').removeClass('header-navigation-catalog-item__headline_active');
            $('.header-navigation-catalog-item__headline').parent().find('div.header-navigation-catalog-item-submenu').removeClass('header-navigation-catalog-item-submenu_active');

            $('.header-navigation-catalog-item__headline').removeClass('header-navigation-catalog__button_focus');
            $('.header-navigation-catalog').removeClass('header-navigation-catalog_active');
        }
    });

});
