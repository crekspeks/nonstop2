$(function() {

    $('.slider-container').owlCarousel({
        items: 1,
        loop: true,
        dots: true,
        nav: true,
        autoplay:true,
        autoplayTimeout:3000,
        navText: ['<img src="img/icons/icon-slider-left.svg" alt="left">', '<img src="img/icons/icon-slider-right.svg" alt="right">'],
        smartSpeed: 900
    });
});
